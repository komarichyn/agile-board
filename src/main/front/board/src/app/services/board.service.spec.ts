import { TestBed, inject } from '@angular/core/testing';

import { BoardService } from './board.service';

describe('BoardServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BoardService]
    });
  });

  it('should be created', inject([BoardService], (service: BoardService) => {
    expect(service).toBeTruthy();
  }));
});
