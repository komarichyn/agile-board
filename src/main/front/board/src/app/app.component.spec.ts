import {TestBed, async} from '@angular/core/testing';

import {AppComponent} from './app.component';
import {BoardComponent} from "./components/board/board.component";
import {BrowserModule} from "@angular/platform-browser";
import {BoardService} from "./services/board.service";
import {DndModule} from "ng2-dnd";
import {FormsModule} from "@angular/forms";

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent, BoardComponent
      ],
      imports: [
        BrowserModule,
        FormsModule,
        DndModule.forRoot()
      ],
      providers: [BoardService]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Agile board');
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to Agile board!!');
  }));
});
