import {Component, OnInit} from '@angular/core';
import {Step} from "../../model/step";
import {Issue} from "../../model/issue";
import {Status} from "../../model/status.enum";


@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  public dragOperation: boolean = false;


  public containers: Array<Step> = [];


  public widgets: Array<Issue> = [];

  public addTo($event: any): void {
    if ($event) {
      this.widgets.push($event.dragData);
    }
  }

  constructor() {
  }

  ngOnInit() {

    this.containers.push(this.getTodoContainer());
    this.containers.push(this.getInProgressContainer());
    this.containers.push(this.getTestingContainer());
    this.containers.push(this.getDoneContainer());

  }

  private getTodoContainer() : Step{
    let step = new Step();
    step.name = 'To Do';
    step.id = 1;
    for(let i =1;i<3;i++ ) {
      let issue = new Issue();
      issue.name = '' + i;
      issue.identifier = "" + i;
      issue.status = Status.TO_DO;
      step.issues.push(issue);
    }
    return step;
  }

  private getInProgressContainer(): Step{
    let step = new Step();
    step.name = 'In Progress';
    step.id = 2;
    for(let i =3;i<5;i++ ) {
      let issue = new Issue();
      issue.name = '' + i;
      issue.identifier = "" + i;
      issue.status = Status.IN_PROGRESS;
      step.issues.push(issue);
    }
    return step;
  }
  private getTestingContainer() : Step{
    let step = new Step();
    step.name = 'Testing';
    step.id = 3;
    for(let i =5;i<9;i++ ) {
      let issue = new Issue();
      issue.name = '' + i;
      issue.identifier = "" + i;
      issue.status = Status.TESTING;
      step.issues.push(issue);
    }
    return step;
  }

  private getDoneContainer(): Step{
    let step = new Step();
    step.name = 'Done';
    step.id = 4;
    return step;
  }

}


