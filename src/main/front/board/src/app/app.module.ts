import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BoardComponent} from "./components/board/board.component";
import {BoardService} from "./services/board.service";
import {FormsModule} from '@angular/forms';
import {DndModule} from 'ng2-dnd';

@NgModule({
  declarations: [
    AppComponent, BoardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    DndModule.forRoot()
  ],
  providers: [BoardService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
