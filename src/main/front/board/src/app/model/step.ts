import {Issue} from "./issue";
export class Step {
  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }
  get issues(): Array<Issue> {
    return this._issues;
  }

  set issues(value: Array<Issue>) {
    this._issues = value;
  }
  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  private _name: string;
  private _id: number;
  private _issues: Array<Issue> = [];


  constructor() {
  }


}
