import {Status} from "./status.enum";
export class Issue {
  get status(): Status {
    return this._status;
  }

  set status(value: Status) {
    this._status = value;
  }
  get identifier(): string {
    return this._identifier;
  }

  set identifier(value: string) {
    this._identifier = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  private _identifier: string;
  private _name: string;
  private _description: string;
  private _status: Status;

  constructor() {
  }
}
